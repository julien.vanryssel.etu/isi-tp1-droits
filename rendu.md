# Rendu TP1 "Les droits d’accès dans les systèmes UNIX"

## Binome

- Vanryssel, Julien, julien.vanryssel.etu@univ-lille.fr

- Vanderlynden, Jarod, jarod.vanderlynden@univ-lille.fr

## Question 1

Création d'un utilisateur appartenant au groupe centos :

adduser -g centos toto

Le processus ne peut pas écrire car il a été lancé par l'utilisateur toto qui dispose simplement du droit de lecture sur le fichier titi.txt

## Question 2

Le caractère x pour un répertoire signifie le droit d'entré dans ce répertoire. 

Création du répertoire mydir avec le droit d'execution enlevé pour le groupe centos :

mkdir mydir 
chmod 765 mydir/

Lorsque l'on esseye d'entrer dasn le répertoire mydir une erreur apparait : bash: cd: mydir: Permission non accordée

On ne peux pas entrer dans le repertoire mydir car on a enlevé le x qui est le droit d'entré dans un répertoire au groupe centos. toto appartient au groupe centos donc il peux lire et écrire mais pas entrer dans le répertoire.

Retour de la commande ls -al mydir : 

ls: impossible d'accéder à mydir/.: Permission non accordée
ls: impossible d'accéder à mydir/..: Permission non accordée
ls: impossible d'accéder à mydir/data.txt: Permission non accordée
total 0
d????????? ? ? ? ?              ? .
d????????? ? ? ? ?              ? ..
-????????? ? ? ? ?              ? data.txt

Comme nous n'avons pas le droit d'entrer dans le répertoire le terminale nous affiche 3 erreur pour dire que nous n'avons pas la permission et nous affiche le retour de la commande ls mais avec des ? presque partout car nous n'avons pas l'autorisation de rentrer dans le fichier donc il est impossible de voir les droit et le reste sur les fichiers qu'il contient. 


## Question 3

Les valeurs des ids sont : UID : 1001, EUID : 1001, GID : 1000, EGID : 1000

Le processus n'arrive pas à ouvrir le fichier data.txt en lecture car il n'a pas accés au répertoire mydir et pour accéder au fichier data.txt il doit absolument rentrer dans le répertoire mais il n'a pas les droits. 

Après activation du flag set-user-id, les valeurs des ids sont : UID : 1001, EUID : 1000, GID : 1000, EGID : 1000

Le processus arrive à ouvrir le fichier data.txt en lecture. C'est toto qui execute le programme mais grâce au flag set-user-id il l'execute avec les droits de l'utilisateur centos qui lui à le droit d'accéder au dossier mydir.

## Question 4

Réponse

## Question 5

- La commande chfn permet de modifier les informations personnel d'un utilisateur.

Commande : ls -al /usr/bin/chfn 
Résultat : -rws--x--x. 1 root root 24048 16 août   2018 /usr/bin/chfn

Les utilisateur du groupe root et les autres pourront exécuter chfn et grâce à l'activation du flag set-user-id, ils pourront exécuter chfn avec les droit de root.

- chfn
Modification des renseignements finger pour toto.
Nom [tata]: toto
Bureau []: M5
Téléphone bureau []: 0616157402
Téléphone domicile []: 0682914631

cat /etc/passwd
toto:x:1001:1000:toto,M5,0616157402,0682914631:/home/toto:/bin/bash

On voit bien que les informations pour toto ont bien été changé dans le fichier passwd.

## Question 6

Les mots de passe utilisateur sont stocké dans le fichier /etc/shadow.
Les mots de passe sont stocké dans /etc/shadow car seul root peut lire ce fichier donc il est impossible pour un utilisateur extérieur qui ne connais pas le mot de passe de root de récuperer le mot de passe. Et même si il parvient à récuperer le mot de passe il est crypté. 

## Question 7

J'ai du créer deux autres utilisateur lambda_a2 et lambda_b2 pour créer des fichier avec ces utilisateur. Cela permet de prouver qu'il est impossible de supprimer ce fichier si on en est pas la propriétaire.

## Question 8



## Question 9



## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








