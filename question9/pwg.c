#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
#include <crypt.h>
#include "check_pass.c"

int ChangePwd(char * username, char * pwdsaisie){
  char *line_buf = NULL;
  size_t line_buf_size = 0;
  ssize_t line_size;
  //J'ouvre le fichier de mot de passe et crée un nouveau fichier
  FILE * f = fopen("/home/admin/passwd.txt", "r");
  FILE * g = fopen("/home/admin/passwd2.txt", "a");
  //La nouvelle ligne
  char *line = malloc(strlen(username) + strlen(pwdsaisie) + 2);
  strcpy(line, username);
  strcat(line,":");
  strcat(line,pwdsaisie);
  strcat(line,"\n");
  //On va jusqu'a la bonne ligne
  char delim[] = ":";
  line_size = getline(&line_buf,&line_buf_size,f);
  char * login = strtok(line_buf, delim);
  while (line_size != -1){
    if(strcmp(login,username)==0){
      //On passe la ligne et on copie la bonne ligne
      fwrite(line, strlen(line), 1, g);
    }
    else{
      //On copie la ligne
      fwrite(line_buf,strlen(line_buf),1,g);
    }
    line_size = getline(&line_buf,&line_buf_size,f);
  }
  fclose(f);
  fclose(g);
  return 0;

}

int NewPwd(char * username, char * pwdsaisie){
  char *line = malloc(strlen(username) + strlen(pwdsaisie) + 2);
  strcpy(line, username);
  strcat(line,":");
  strcat(line,pwdsaisie);
  strcat(line,"\n");
  FILE * f = fopen("/home/admin/passwd.txt", "a");
  fwrite(line, strlen(line), 1, f);
  fclose(f);
  return 0;
}



int main(){
  char * clef = "13E04J01M";
  char * pwdcrypt;
  char * passwd;
  char pwdsaisie[30];
  int UID = getuid();
  char * username;
  char * n;
  struct passwd *pwd;
  if ((pwd = getpwuid(UID)) != NULL) {
    username = pwd->pw_name;
  }
  FILE * f = fopen("/home/admin/passwd.txt", "r");
  if (f == NULL) {
    perror("Cannot open file");
    exit(EXIT_FAILURE);
  }
  passwd = getpasswd(f,username);
    //Si l'utilisateur a déjà un mot de passe
  if (passwd){
    n = strchr(passwd,'\n');
    *n = '\0';
    strncpy(pwdsaisie, getpass("Entrer votre mot de passe : \n "), 30);
    //Si le mdp est bon
    pwdcrypt = crypt(pwdsaisie,clef);
    if (strcmp(passwd,pwdcrypt) == 0){
      //Demander a l'utilisateur un new mdp
      strncpy(pwdsaisie, getpass("Entrer votre nouveau mot de passe : \n "), 30);
      pwdcrypt = crypt(pwdsaisie,clef);
      ChangePwd(username,pwdcrypt);
      remove("/home/admin/passwd.txt");
      rename("/home/admin/passwd2.txt","/home/admin/passwd.txt");
    }
    //Sinon
    else{
      printf("Mauvais mot de passe");
      return 1;
    }
    
  }
  //Si l'utilisateur n'a pas de mot de passe, il en définit un tout de suite
  else{
    strncpy(pwdsaisie, getpass("Entrer un mot de passe : \n "), 30);
    pwdcrypt = crypt(pwdsaisie,clef);
    NewPwd(username,pwdcrypt);
  }
}
