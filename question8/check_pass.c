#include "check_pass.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

char* getpasswd(FILE *f,char* user){

    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    char *line_buf = NULL;
    size_t line_buf_size = 0;
    ssize_t line_size;
    char delim[] = ":";
    char * passwd;
    line_size = getline(&line_buf, &line_buf_size, f);
    char * login = strtok(line_buf, delim);
    if(strcmp(login,user) == 0){
           passwd = strtok(NULL,delim);
           return passwd;
    }
    while (line_size != -1){
        line_size = getline(&line_buf, &line_buf_size, f);
         login = strtok(line_buf, delim);
         if(strcmp(login,user)){
            passwd = strtok(NULL,delim);
            return passwd;
        }
    }
    return "";
}