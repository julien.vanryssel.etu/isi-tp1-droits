#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>
//#include "check_pass.c"



int main(int argc, char *argv[]) {
    
    struct stat sb;
    char pwdsaisie[30];
    char * username;
    char * pwd;
    int UID = getuid();
    int GID =  getgid();
    char * n;

    struct passwd *passwd;
    if ((passwd = getpwuid(UID)) != NULL) {
        username = passwd->pw_name;
    } 
    
    if(stat(argv[1], &sb) == -1) {
        perror("stat");
        exit(EXIT_SUCCESS);
    }
    if(GID != sb.st_gid){
        perror("groupe non indentique");
        exit(EXIT_SUCCESS);
    }
    if( UID != sb.st_uid){
        perror("Vous n'avez pas les droit pour modifier ce fichier");
        exit(EXIT_SUCCESS);
    }
    FILE *f = fopen("/home/admin/passwd.txt", "r");
    pwd = getpasswd(f,username);
    if(pwd) {
        n = strchr(pwd,'\n');
        *n = '\0';
        strncpy(pwdsaisie, getpass("Entrer votre mot de passe : \n "), 30);  
        if(strcmp(pwdsaisie,pwd) == 0){
            remove(argv[1]);
        }else {
            perror("Mot de passe incorect");
            exit(EXIT_FAILURE);
        }
    }else {
        perror("Vous n'avez pas de mot de passe pour cet utilisateur");
        exit(EXIT_FAILURE);
    }